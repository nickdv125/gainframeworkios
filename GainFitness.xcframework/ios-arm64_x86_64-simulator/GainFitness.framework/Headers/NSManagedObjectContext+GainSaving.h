//
//  NSManagedObject+GainSaving.h
//  GainFitness
//
//  Created by Nicholas Della Valle on 9/21/20.
//  Copyright © 2020 GAIN Fitness. All rights reserved.
//

#import <CoreData/CoreData.h>

typedef void (^SaveCompletionHandler)(BOOL contextDidSave, NSError * __nullable error);

NS_ASSUME_NONNULL_BEGIN

@interface NSManagedObjectContext (GainSaving)
- (void)MR_gainSaveToPersistentStoreAndWait;
- (void)MR_gainSaveWithOptions:(NSUInteger)saveOptions completion:(__nullable SaveCompletionHandler)completion;
- (void)MR_configureAsGainRootSavingContext;
+ (void) MR_setGainDefaultContext:(NSManagedObjectContext *)moc rootContext:(NSManagedObjectContext *)root;
@end



NS_ASSUME_NONNULL_END
