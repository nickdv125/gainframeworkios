//
//  GainFitness.h
//  GainFitness
//
//  Created by Nicholas Della Valle on 8/9/20.
//  Copyright © 2020 GAIN Fitness. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "GainWorkoutListTableViewController.h"
#import "NSManagedObjectContext+GainSaving.h"
#import "GainEventsDelegate.h"

//! Project version number for GainFitness.
FOUNDATION_EXPORT double GainFitnessVersionNumber;

//! Project version string for GainFitness.
FOUNDATION_EXPORT const unsigned char GainFitnessVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <GainFitness/PublicHeader.h>


