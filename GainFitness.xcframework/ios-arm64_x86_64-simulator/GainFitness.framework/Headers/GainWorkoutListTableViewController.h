//
//  SGWorkoutListTableViewController.h
//  GainClient
//
//  Created by Sam Goldstein on 8/14/15.
//  Copyright (c) 2015 Gain Fitness. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol GainWorkoutListDelegate  <NSObject>
-(void)gainWorkoutListExitPressed;
@end

@protocol GainEventsDelegate;

@interface GainWorkoutListTableViewController : UIViewController

@property (nonatomic, weak) id  <GainWorkoutListDelegate> __nullable delegate;
@property (nonatomic, weak) id <GainEventsDelegate> __nullable eventsDelegate;




@end
