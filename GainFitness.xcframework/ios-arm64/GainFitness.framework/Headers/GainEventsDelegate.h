//
//  GainEventsDelegate.h
//  GainFitness
//
//  Created by Nicholas Della Valle on 9/2/20.
//  Copyright © 2020 GAIN Fitness. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@protocol GainEventsDelegate  <NSObject>
-(void)loggedEventNamed:(NSString * __nonnull)name parameters:(NSDictionary * __nullable)params;
@end

NS_ASSUME_NONNULL_END
