## Introduction

The Gain Fitness iOS SDK is a xcframework written in Objective-C & Swift.  It allows your application to provide the GAIN Fitness workout mode experience to your apps users.  An API Key is required.  Please contact support-sdk@gainfitness.com to obtain an API Key.  



## Requirements

Your applications Minimum iOS Deployment target must be set to iOS 12.0 or higher.

Your workspace must use the New build system since the GAIN Fitness SDK is built as an xcframework.  You can check this in XCode under File->Workspace Settings->Build System.  



## Installation

Download the latest Gain Fitness Framework Release (Tagged) from this repository.

Unzip this file, which contains GainFitness.framework.

Copy GainFitness.framework to a directory somewhere in the hierarchy associated with your project's .xcodeproj file.

In Xcode's Project Navigator (far left column), select the top item (which corresponds to the project itself).

Under Targets, select your app.

On the General tab, find the Frameworks, Libraries, and Embedded Content section. In it, add GainFitness.framework by clicking + > Add Other > Add Files and selecting the framework.



## Signing and Capabilities

Make sure your app has the Audio, AirPlay, and Picture in Picture Background Mode capibility set under your app Targets Signings and Capabilities since the Gain Workout player required background audio.



## Usage

The GainFitness SDK home screen is the GainWorkoutListTableViewController, which when displayed, will download the workouts.
Prior to creating an instance of GainWorkoutListTableViewController, initialize the SDK with your API Key (required), a color (UIColor)
which themes the SDK for your brand, and (optionally) a delegate to handle errors coming from the SDK

**Swift**
```swift
GainFitnessSDK.initialize(apiKey: "<YOUR_API_KEY>", themeColor: <YOUR_THEME_COLOR>, errorDelegate:nil)
```

**Objective-C**
```objective_c
[GainFitnessSDK initializeWithApiKey:@"<YOUR_API_KEY>" themeColor:<YOUR_THEME_COLOR> errorDelegate:nil];
```





Instantiate the GainWorkoutListTableViewController

**Swift**
```swift
let workouts = GainWorkoutListTableViewController()
```

**Objective-C**
```objective_c
GainWorkoutListTableViewController *workoutlist = [[GainWorkoutListTableViewController alloc] init];
```





Set the delegate (GainWorkoutListDelegate).  

```swift
workouts.delegate = self
```


This will require you to implement the gainWorkoutListExitPressed method in order to handle users request to exit the Gain Module. Example conformance to the GainWorkoutListDelegate below.

**Swift**
```swift

extension MyViewController: GainWorkoutListDelegate {
	
	func gainWorkoutListExitPressed() {
		self.navigationController?.popViewController(animated: true)
	}
	
}
```

**Objective-C**
```objective_c

-(void)gainWorkoutListExitPressed {
	[self.navigationController popViewControllerAnimated:true];
}
```





(Optional) If you plan to handle analytics events coming from the Gain SDK, you will be required to also set and conform to the GainEventsDelegate.

```swift
workouts.eventsDelegate = self
```

**Swift**
```swift
extension MyViewController: GainEventsDelegate {
	
	func loggedEventNamed(_ name: String, parameters params: [AnyHashable : Any]?) {
		print("Event logged \(name)" + (params?.description ?? ""))
	}
}

```

**Objective-C**
```objective_c
-(void)loggedEventNamed:(NSString *)name parameters:(NSDictionary *)params {
	
}

```




## Events

The Gain SDK sends the following events

gainInitialized - Gain module was activated (i.e. Workout list was shown) - No Parameters<br />
gainViewedWorkoutPreview - A workout was selected from the Workoust list - Parameters: workoutName<br />
gainStartedWorkout - A workout was started - Parameters: workoutName<br />
gainStartedCompleted - A workout was completed - Parameters: workoutName, appState (background or active)



## Known Issues

1. When distributing your app using Ad hoc distribution, Pleae UNCHECK "Rebuid from bitcode" on the Ad hoc Distrubtion options screen.  This is currently an issue we are working to resolve and could be an Apple issue. No issues have been encoutered with App Store distribution with bitcode.

